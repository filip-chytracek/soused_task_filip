import React, { Component } from 'react';

import './App.css';
import GenerateObject from './GenerateObject/GenerateObject.js';

const demoData = {
	type: 'text',
	label: 'name',
	placeholder: 'Please fill your name'
};

const demoData1 = {
	type: 'text1',
	label: 'name1',
	placeholder: 'Please fill your name1'
};

class App extends Component {
	state = {
		items: [ demoData, demoData1 ],
		inputValue: null
	};

	addField = (item) => {
		this.setState({
			items: [ ...this.state.items, item ]
		});
	};

	render() {
		return (
			<React.Fragment>
				<GenerateObject action={this.addField} />
				<button
					onClick={() => {
						console.log(this.state.items);
					}}
				/>
				{this.state.input}
			</React.Fragment>
		);
	}
}

export default App;
