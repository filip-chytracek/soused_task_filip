import React, { Component } from 'react';

import DropdownFunctions from './DropdownFunctions/DropdownFunctions';

import './GenerateObject.css';

class GenerateObject extends Component {
	state = {
		active: null,
		dropdownTitles: [ 'checkbox', 'button', 'input' ],
		input: ''
	};

	generateComponent = (caption) => {
		this.setState({ title: caption, active: null });
	};

	dropdownStyleHandler = () => {
		this.setState({ active: !this.state.active ? 'block' : null });
	};

	getInputData = (titleText) => {
		this.setState({ input: [ ...this.state.input, <input type={this.state.title} /> ] });
		console.log('Text of input', titleText);
	};

	render() {
		console.log(this.props);
		return (
			<div className="wrapper">
				<div className="dropdown">
					<button
						className="btn btn-default dropdown-toggle"
						type="button"
						id="dropdownMenu1"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="true"
						onClick={this.dropdownStyleHandler}
					>
						Dropdown
						<span className="caret" />
					</button>
					<DropdownFunctions.AddItemToDropdown
						action={this.generateComponent}
						dropdownTitles={this.state.dropdownTitles}
						status={this.state.active}
						getInputData={this.getInputData}
					/>
					{this.state.input}
					<div className="dropdownWrapper" />
				</div>
			</div>
		);
	}
}

export default GenerateObject;
