import React, { Component } from 'react';

import './DropdownFunctions.css';

class AddItemToDropdown extends Component {
	state = {
		input: null
	};

	changeInputText = (e) => {
		this.setState({
			inputText: e.currentTarget.value
		});
	};

	render() {
		return (
			<div>
				<div className="wrapperDropdown">
					<ul
						className="dropdown-menu"
						style={{ display: this.props.status }}
						aria-labelledby="dropdownMenu1"
					>
						{this.props.dropdownTitles.map((title) => {
							return (
								<div key={title}>
									<li onClick={() => this.props.action(title)}>{title}</li>
									<li role="separator" className="divider" />
									<div />
								</div>
							);
						})}
					</ul>
				</div>

				<div>
					<form>
						<input
							onChange={this.changeInputText}
							style={{ width: '800px', height: '150px' }}
							placeholder="Type your text"
						/>
					</form>
					<button className="" onClick={() => this.props.getInputData(this.state.inputText)}>
						Submit
					</button>
				</div>
			</div>
		);
	}
}

const functions = {
	AddItemToDropdown
};

export default functions;
